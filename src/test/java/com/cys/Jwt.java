package com.cys;


import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import org.junit.jupiter.api.Test;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class Jwt {
    @Test
    public void testJwt() {
        HashMap<String, Object> jwtHashMap = new HashMap<>();
        jwtHashMap.put("id", 1);
        jwtHashMap.put("username", "zhangsan");
        String token = JWT.create()
                .withClaim("user", jwtHashMap)
                .withExpiresAt(new Date(System.currentTimeMillis() + 1000 * 60 * 60))
                .sign(Algorithm.HMAC256("cyshuang"));
        System.out.println(token);
    }

    @Test
    public void testPares() {
        String token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2OTk5NTU0NjcsInVzZXIiOnsiaWQiOjEsInVzZXJuYW1lIjoiemhhbmdzYW4ifX0.B6auGpJvL9s7E121vRD7h2m-IjHWx1524QXOhrf-frQ";
        JWTVerifier cyshuang = JWT.require(Algorithm.HMAC256("cyshuang")).build();
        DecodedJWT verify = cyshuang.verify(token);
        Map<String, Claim> claim = verify.getClaims();
        System.out.println(claim.get("user"));
    }
}
