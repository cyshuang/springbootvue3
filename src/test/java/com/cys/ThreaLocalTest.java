package com.cys;

import org.junit.jupiter.api.Test;

public class ThreaLocalTest {
    @Test
    public void testThreadLocal() {
        ThreadLocal threadLocal = new ThreadLocal();
        new Thread(()->{
            threadLocal.set("wzh");
            System.out.println(Thread.currentThread().getName() + ":" + threadLocal.get());
            System.out.println(Thread.currentThread().getName() + ":" + threadLocal.get());
            System.out.println(Thread.currentThread().getName() + ":" + threadLocal.get());
        },"傻逼").start();
        new Thread(()->{
            threadLocal.set("cys");
            System.out.println(Thread.currentThread().getName() + ":" + threadLocal.get());
            System.out.println(Thread.currentThread().getName() + ":" + threadLocal.get());
            System.out.println(Thread.currentThread().getName() + ":" + threadLocal.get());
        },"帅哥").start();

    }

}
