package com.cys.controller;

import com.cys.mapper.RoleMapper;
import com.cys.mapper.RoleMenuMapper;
import com.cys.pojo.Admin;
import com.cys.pojo.Menu;
import com.cys.service.AdminService;
import com.cys.service.IMenuService;
import com.cys.utils.JwtUtils;
import com.cys.utils.R;
import com.cys.utils.ThreadLocalUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@RestController
public class AdminController {
    @Autowired
    private AdminService adminService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private RoleMapper roleMapper;

    @Autowired
    private RoleMenuMapper roleMenuMapper;
    @Autowired
    private IMenuService menuService;
    @PostMapping("/login")
    public R login(@RequestBody Admin user) {
        Admin dbUser = adminService.findByUserName(user.getUsername());
        if (dbUser != null && dbUser.getPassword().equals(user.getPassword())) {
            //dbUser.setToken(JwtUtils.generateToken(dbUser));
            HashMap<String, Object> claims = new HashMap<>();
            claims.put("id", dbUser.getId());
            claims.put("username", dbUser.getUsername());
//            String token = JwtUtils.createToken(claims);
            dbUser.setToken(JwtUtils.createToken(claims));
            dbUser.setPassword("");
            stringRedisTemplate.opsForValue().set("token", dbUser.getToken(), 1000 * 60 * 60, TimeUnit.SECONDS);

            String role = dbUser.getRole();
            Integer roleId = roleMapper.selectByRole(role);
            List<Integer> menuIds = roleMenuMapper.getRoleMenu(roleId);
            List<Menu> menuList = menuService.listByIds(menuIds);
            Map<String, Object> result = new HashMap<>();
            result.put("data", dbUser);
            result.put("menus", menuList);

            List<Menu> menus = menuService.findMenus();
            ArrayList<Menu> roleMenus = new ArrayList<>();
            for (Menu menu : menus) {
                if(menuIds.contains(menu.getId())){
                    roleMenus.add(menu);
                }
                List<Menu> children = menu.getChildren();
                children.removeIf(child->!menuIds.contains(child.getId()));
            }
            dbUser.setMenus(roleMenus);
            return R.success(result);
        } else {
            return R.error(-1, "用户名或密码错误");
        }
    }

    @GetMapping("/info")
    public R inFo() {
        Map<Object, String> map = ThreadLocalUtil.get();
        String username = map.get("username");
        System.out.println();
        return R.success(adminService.findByUserName(username)+"----"+stringRedisTemplate.opsForValue().get("token"));
    }
}
