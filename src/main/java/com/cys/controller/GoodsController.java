package com.cys.controller;

import com.cys.pojo.Goods;
import com.cys.service.GoodsService;
import com.cys.utils.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/goods")
public class GoodsController {
    @Autowired
    private GoodsService goodsService;
@GetMapping("/list")
    public R getGoodsList() {
        return R.success(goodsService.list());
    }


    @GetMapping("/pagination")
    public R pagination(Integer currentPage, Integer pageSize) {
        return goodsService.pagination(currentPage, pageSize);
    }

    @PostMapping("/updateGoods")
    public R updateGoods(@RequestBody Goods goods){
        return goodsService.updateGoods(goods);
    }
    @DeleteMapping("/delete/{id}")
    public R delete(@PathVariable Long id){
        goodsService.removeById(id);
            return R.success();
    }
}
