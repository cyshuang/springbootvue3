package com.cys.controller;

import com.cys.pojo.User;
import com.cys.service.UserService;
import com.cys.utils.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;

    @GetMapping("/pagination")
    public R pagination(Integer currentPage, Integer pageSize, String username, Boolean status, String email) {
        return userService.pagination(currentPage, pageSize, username, status, email);
    }

    @PostMapping("/addUser")
    public R addUser(@RequestBody User user) {
        return userService.addUser(user);
    }

    @GetMapping("/upStatus")
    public R upStatus(Boolean status, Integer id) {
        return userService.upStatus(status, id);
    }

    @DeleteMapping("/delete/{id}")
    public R delete(@PathVariable Long id) {
        boolean b = userService.removeById(id);
        if (b) {
            return R.success("删除成功");
        }
        return R.error(-1, "删除失败");
    }

    @PostMapping("/delBatch")
    public R delBatch(@RequestBody List<Long> ids){
        if (userService.removeBatchByIds(ids)) {
            return R.success("批量删除成功！");
        }
        return R.error(-1,"批量删除失败！");
    }
}
