package com.cys.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cys.pojo.Role;
import com.cys.service.IRoleService;
import com.cys.utils.R;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;


/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author 张明鹏
 * @since 2023-11-02
 */
@RestController
@RequestMapping("/role")
public class RoleController {

    @Resource
    private IRoleService roleService;

    @PostMapping("/save")
    public R save(@RequestBody Role role) {
        if (roleService.saveOrUpdate(role)) {
            return R.success("操作成功");
        }
        return R.error(-1,"操作失败");
    }

    @DeleteMapping("del/{id}")
    public R delete(@PathVariable Integer id) {
        if (roleService.removeById(id)) {
            return R.success("删除成功");
        }
        return R.error(-1,"删除失败");
    }

    @PostMapping("/del/batch")
    public R deleteBatch(@RequestBody List<Integer> ids) {
        if (roleService.removeByIds(ids)) {
            return R.success("批量删除成功");
        }
        return R.error(-1,"批量删除失败");
    }

    @GetMapping("/list")
    public List<Role> findAll() {
        return roleService.list();
    }

    @GetMapping("/findOne/{id}")
    public R findOne(@PathVariable Integer id) {
        List<Role> list = roleService.list();
        return R.success(list);
    }

    @GetMapping("/page")
    public R findPage(@RequestParam Integer pageNum,
                           @RequestParam Integer pageSize,
                           @RequestParam(defaultValue = "") String name) {
        QueryWrapper<Role> queryWrapper = new QueryWrapper<>();
        queryWrapper.like("name",name);
        queryWrapper.orderByDesc("id");
        Page<Role> page = roleService.page(new Page<>(pageNum, pageSize),queryWrapper);
        long count = roleService.count();
        HashMap<String, Object> map = new HashMap<>();
        map.put("data", page);
        map.put("total", count);
        return R.success(map);
    }

    @PostMapping("/updateRoleMenu/{roleId}")
    public R  updateRoleMenu(@PathVariable Integer roleId,@RequestBody List<Integer> menuIds) {
        if (roleService.updateRoleMenu(roleId, menuIds)) {
            return R.success("更新成功");
        }
        return R.error(-1,"更新失败");
    }

    @GetMapping("/getRoleMenu/{roleId}")
    public R getRoleMenu(@PathVariable Integer roleId){
        List<Integer> roleMenu = roleService.getRoleMenu(roleId);
        return R.success(roleMenu);
    }
}

