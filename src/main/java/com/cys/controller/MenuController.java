package com.cys.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import com.cys.mapper.IconMapper;
import com.cys.pojo.Icon;
import com.cys.pojo.Menu;
import com.cys.service.IMenuService;
import com.cys.utils.R;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;


/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author 张明鹏
 * @since 2023-11-02
 */
@RestController
@RequestMapping("/menu")
public class MenuController {

    @Resource
    private IMenuService menuService;
    @Resource
    private IconMapper iconMapper;

    @PostMapping("/save")
    public R save(@RequestBody Menu menu) {
        if (menuService.saveOrUpdate(menu)) {
            return R.success("操作成功");
        }
        return R.error(-1,"操作失败");
    }

    @DeleteMapping("del/{id}")
    public R delete(@PathVariable Integer id) {
        if (menuService.removeById(id)) {
            return R.success("删除成功");
        }
        return R.error(-1,"删除失败");
    }

    @PostMapping("/del/batch")
    public R deleteBatch(@RequestBody List<Integer> ids) {
        if (menuService.removeByIds(ids)) {
            return R.success("批量删除成功");
        }
        return R.error(-1,"批量删除失败");
    }

    @GetMapping("/list")
    public R findAll() {
        return R.success(menuService.findMenus());
    }

    @GetMapping("/findOne/{id}")
    public R findOne(@PathVariable Integer id) {
        List<Menu> list = menuService.list();
        return R.success(list);
    }

    @GetMapping("/page")
    public R findPage(@RequestParam Integer pageNum,
                           @RequestParam Integer pageSize,
                           @RequestParam(defaultValue = "") String name) {
        QueryWrapper<Menu> queryWrapper = new QueryWrapper<>();
        queryWrapper.like("name", name);
        queryWrapper.orderByDesc("id");
        Page<Menu> page = menuService.page(new Page<>(pageNum, pageSize), queryWrapper);
        long count = menuService.count();
        HashMap<String, Object> map = new HashMap<>();
        map.put("data", page);
        map.put("total", count);
        return R.success(map);
    }

    @GetMapping("/icon")
    public R icon(){
        QueryWrapper<Icon> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("type","icon");
        List<Icon> icons = iconMapper.selectList(queryWrapper);
        return R.success(icons);
    }
}

