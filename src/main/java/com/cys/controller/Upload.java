package com.cys.controller;


import com.cys.utils.AliOSSUtils;
import com.cys.utils.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@RequestMapping("/file")
public class Upload {
    private final AliOSSUtils aliOSSUtils;

    @Autowired
    public Upload(AliOSSUtils aliOSSUtils) {
        this.aliOSSUtils = aliOSSUtils;
    }

    @PostMapping("/upload")
    public R<?> upload(MultipartFile file) throws IOException {
        String upload = aliOSSUtils.upload(file);
        return R.success(upload);
    }


}
