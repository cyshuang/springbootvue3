package com.cys.handler;

import com.cys.utils.R;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ExceptionHandler;
    import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class GlobalExceptionHandler {
    @ExceptionHandler(Exception.class)
    public R handleException(Exception e){
        e.printStackTrace();
        return R.error(-1, StringUtils.hasLength(e.getMessage())?e.getMessage():"操作失败");
    }
}
