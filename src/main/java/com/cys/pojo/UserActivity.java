package com.cys.pojo;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import java.util.Date;


/**
 * 用户活动表
 */
@TableName("user_activity")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserActivity {
    @TableId(value = "id",type = IdType.AUTO)
    private Long id;
    private Long activityId;
    private Long userId;
    @TableField(fill = FieldFill.INSERT)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime; //加入时间
}
