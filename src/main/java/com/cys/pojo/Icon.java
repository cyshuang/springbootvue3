package com.cys.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("tb_icon")
public class Icon {
    private  String name;
    private String icon;
    private String type;
}
