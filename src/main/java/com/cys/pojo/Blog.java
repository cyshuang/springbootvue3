package com.cys.pojo;

import com.baomidou.mybatisplus.annotation.*;

import java.time.LocalDateTime;
import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * <p>
 * 游记表
 * </p>
 *
 * @author 王正海
 * @since 2023-07-26
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class Blog implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 用户id
     */
    private Long userId;

    /**
     * 用户头像
     */
    @TableField(exist = false)
    private String avatar;
    /**
     * 用户名
     */
    @TableField(exist = false)
    private String username;
    /**
     * 博客浏览量
     */
    @TableField(exist = false)
    private Long browse;
    /**
     * 是否点赞过了
     */
    @TableField(exist = false)
    private Boolean isLike;
    /**
     * 标题
     */
    private String title;

    /**
     * 游记照片，最多9张，多张以","隔开
     */
    private String images;

    /**
     * 游记的文字描述
     */
    private String content;

    /**
     * 点赞数量
     */
    private Integer liked;

    /**
     * 游记标签最多5个，多个以","隔开
     */
    private String tag;

    /**
     * 评论数量
     */
    private Integer comments;

    @TableField(fill = FieldFill.INSERT)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime; //创建时间
    @TableField(fill = FieldFill.INSERT_UPDATE)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date updateTime;//修改时间
    @TableLogic
    private Integer deleted;

}
