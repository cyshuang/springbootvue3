package com.cys.pojo;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@TableName("video_comments")
public class VideoComments implements Serializable {
    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 评论内容
     */
    private String content;
    /**
     * 点赞
     */
    private Integer liked;

    /**
     * 用户名
     */
    @TableField(exist = false)
    private String username;

    /**
     * 用户ID
     */
    private Long userId;

    /**
     * 用户头像
     */
    @TableField(exist = false)
    private String avatar;

    /**
     * 业务模块ID
     */
    private Long foreignId;

    /**
     * 回复的对象
     */
    private String targetUserId;

    /**
     * 父级评论ID
     */
    private Long parentId;

    /**
     * 子级评论集合
     */
    @TableField(exist = false)
    private List<VideoComments> children;

    @TableField(fill = FieldFill.INSERT)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime; //创建时间

    @TableField(fill = FieldFill.INSERT_UPDATE)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date updateTime;//修改时间
}
