package com.cys.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
//景点表
public class Scenic {
    @TableId(value = "id",type = IdType.AUTO)
    private Long id;
    private String url;
    private String city;
    private Long hot; //热度
}
