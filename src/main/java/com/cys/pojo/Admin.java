package com.cys.pojo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.List;

@Data
@TableName(value = "Admin")
public class Admin {
    private Integer id;
    private String username;
    private String password;
    private String avatar;
    private String token;
    private String role;
    @TableField(exist = false)
    private List<Menu> Menus;
}
