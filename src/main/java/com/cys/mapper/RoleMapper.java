package com.cys.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import com.cys.pojo.Role;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author 张明鹏
 * @since 2023-11-02
 */
@Mapper
public interface RoleMapper extends BaseMapper<Role> {

    @Select("select id from tb_role where flag=#{flag}")
    Integer selectByRole(@Param("flag") String flag);
}
