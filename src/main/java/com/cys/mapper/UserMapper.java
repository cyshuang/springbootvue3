package com.cys.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cys.pojo.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Update;

@Mapper
public interface UserMapper extends BaseMapper<User> {
    @Update("update user set status =#{status}  where id=#{id} ;")
    void upStatus(Boolean status, Integer id);
}
