package com.cys.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cys.pojo.Icon;

public interface IconMapper extends BaseMapper<Icon> {
}
