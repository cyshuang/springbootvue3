package com.cys.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cys.pojo.Admin;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface AdminMapper extends BaseMapper<Admin> {
    @Select("select * from travel.admin where username=#{username}")
    Admin findByUserName(String username);
}
