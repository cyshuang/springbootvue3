package com.cys.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import com.cys.pojo.Menu;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author 张明鹏
 * @since 2023-11-02
 */
@Mapper
public interface MenuMapper extends BaseMapper<Menu> {

    @Select("select *from tb_menu where id=#{menuId}")
    Menu getById(@Param("menuId") Integer menuId);
}
