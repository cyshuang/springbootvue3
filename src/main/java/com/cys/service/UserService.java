package com.cys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cys.pojo.User;
import com.cys.utils.R;

public interface UserService extends IService<User> {
    R pagination(Integer currentPage, Integer pageSize, String username, Boolean status, String email);

    R addUser(User user);

    R upStatus(Boolean status, Integer id);
}
