package com.cys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cys.pojo.Menu;


import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author 张明鹏
 * @since 2023-11-02
 */
public interface IMenuService extends IService<Menu> {

    List<Menu> findMenus();
}
