package com.cys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cys.pojo.Admin;

public interface AdminService extends IService<Admin> {
    Admin findByUserName(String username);
}
