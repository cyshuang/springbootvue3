package com.cys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cys.pojo.Goods;
import com.cys.utils.R;

public interface GoodsService extends IService<Goods> {
    R pagination(Integer currentPage, Integer pageSize);

    R updateGoods(Goods goods);
}
