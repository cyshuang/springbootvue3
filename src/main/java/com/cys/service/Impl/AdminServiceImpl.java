package com.cys.service.Impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cys.mapper.AdminMapper;
import com.cys.pojo.Admin;
import com.cys.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AdminServiceImpl extends ServiceImpl<AdminMapper,Admin> implements AdminService{
    @Autowired
    private AdminMapper adminMapper;

    @Override
    public Admin findByUserName(String username) {
        return adminMapper.findByUserName(username);
    }
}
