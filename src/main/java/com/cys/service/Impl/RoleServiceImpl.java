package com.cys.service.Impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cys.mapper.MenuMapper;
import com.cys.mapper.RoleMapper;
import com.cys.mapper.RoleMenuMapper;
import com.cys.pojo.Role;
import com.cys.pojo.RoleMenu;
import com.cys.service.IRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author 张明鹏
 * @since 2023-11-02
 */
@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements IRoleService {


    @Autowired
    private RoleMenuMapper roleMenuMapper;
    @Autowired
    private MenuMapper menuMapper;
    @Transactional
    @Override
   public boolean updateRoleMenu(Integer roleId, List<Integer> menuIds) {
// 1.根据角色id删除角色菜单
       roleMenuMapper.deleteByRoleId(roleId);
// 2.根据角色id查询菜单id
// 2.根据角色id和菜单id构建角色菜单对象
// 3.循环构建角色菜单对象
        for (Integer menuId : menuIds) {
 //        Menu menu = menuMapper.getById(menuId);
//         if(menu.getPid()!=null && !menuIds.contains(menu.getPid())){
//             RoleMenu roleMenu = new RoleMenu();
//             roleMenu.setRoleId(roleId);
//             roleMenu.setMenuId(menu.getPid());
//             roleMenuMapper.insert(roleMenu);
//         }
// 3.根据角色id和菜单id构建角色菜单对象
            RoleMenu roleMenu = new RoleMenu();
            roleMenu.setRoleId(roleId);
            roleMenu.setMenuId(menuId);
// 5.插入角色菜单
            roleMenuMapper.insert(roleMenu);
        }
// 5.返回true
        return true;
    }

    @Override
    public List<Integer> getRoleMenu(Integer roleId) {
        return roleMenuMapper.getRoleMenu(roleId);
    }
}
