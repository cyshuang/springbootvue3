package com.cys.service.Impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cys.mapper.UserMapper;
import com.cys.pojo.User;
import com.cys.service.UserService;
import com.cys.utils.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {
    @Autowired
    private UserMapper userMapper;

    @Override
    public R pagination(Integer currentPage, Integer pageSize, String username, Boolean status, String email) {
        IPage<User> page = new Page<>(currentPage, pageSize);
        QueryWrapper<User> wrapper = new QueryWrapper<>();
        if (!"".equals(username)) {
            wrapper.like("username", username);
        }
        if (!"".equals(email)) {
            wrapper.like("email", email);
        }
        if (null != status) {
            wrapper.eq("status", status);
        }
        return R.success(userMapper.selectPage(page, wrapper));
    }

    @Override
    public R addUser(User user) {
        if (user.getId() == null) {
            if ("".equals(user.getEmail()) || null == user.getEmail()) {
                return R.error(-1, "邮箱不能为空");
            }
            QueryWrapper<User> wrapper = new QueryWrapper<>();
            wrapper.eq("email", user.getEmail());
            if (userMapper.selectCount(wrapper) > 0) {
                return R.error(-1, "该邮箱已存在");
            }

            String username = user.getUsername();
            if (username.equals("")) {
                return R.error(-1, "用户名不能为空");
            }
            int i = userMapper.insert(user);
            if (i > 0) {
                return R.success("添加成功");
            }
            return R.error(-1, "添加失败");
        } else {
            int i = userMapper.updateById(user);
            if (i > 0) {
                return R.success("修改成功");
            }
            return R.error(-1, "修改成功");
        }
    }

    @Override
    public R upStatus(Boolean status, Integer id) {
        userMapper.upStatus(status, id);
        return R.success("操作成功");
    }
}
