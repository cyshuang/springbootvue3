package com.cys.service.Impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cys.mapper.GoodsMapper;
import com.cys.pojo.Goods;
import com.cys.service.GoodsService;
import com.cys.utils.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GoodsServiceImpl extends ServiceImpl<GoodsMapper, Goods> implements GoodsService {
    @Autowired
    private GoodsMapper goodsMapper;

    @Override
    public R pagination(Integer currentPage, Integer pageSize) {
        IPage<Goods> page = new Page<>(currentPage, pageSize);
        QueryWrapper<Goods> wrapper = new QueryWrapper<>();
        return R.success(baseMapper.selectPage(page, wrapper));
    }

    @Override
    public R updateGoods(Goods goods) {
        if (goods.getId() != null) {
            if (goodsMapper.updateById(goods) > 0) {
                return R.success("修改成功");
            } else  R.error(-1, "修改失败");
        }
        int i = goodsMapper.insert(goods);
        if (i > 0) {
            return R.success("添加成功");
        }
        return R.error(-1, "添加失败");
    }

}
