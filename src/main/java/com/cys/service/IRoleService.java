package com.cys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cys.pojo.Role;


import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author 张明鹏
 * @since 2023-11-02
 */
public interface IRoleService extends IService<Role> {

    boolean updateRoleMenu(Integer roleId, List<Integer> menuIds);

    List<Integer> getRoleMenu(Integer roleId);
}
