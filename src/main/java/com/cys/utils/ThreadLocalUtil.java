package com.cys.utils;


public class ThreadLocalUtil {
    //提供对象
    private static final ThreadLocal<Object> THREAD_LOCAL = new ThreadLocal<>();

    //根据键取值
    public static <T> T get() {
        return (T) THREAD_LOCAL.get();
    }

    public static void set(Object value) {
        THREAD_LOCAL.set(value);
    }

    //清楚防止内存泄露
    public static void remove() {
        THREAD_LOCAL.remove();
    }


}
