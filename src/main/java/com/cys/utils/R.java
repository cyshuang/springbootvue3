package com.cys.utils;

import lombok.Data;

@Data
public class R<T> {
    private Integer code;
    private String msg;
    private T data;

    public R() {
    }

    public R(T data) {
        this.data = data;
    }

    public static <T> R<T> success(T data) {
        R<T> result = new R<>(data);
        result.setCode(0);
        result.setMsg("success");
        return result;
    }

    public static R success() {
        R result = new R<>();
        result.setCode(0);
        result.setMsg("success");
        return result;
    }

    public static R error(Integer code, String msg) {
        R result = new R();
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

}
